import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { fromEvent, of } from 'rxjs';
import { map, filter, debounceTime, switchAll } from 'rxjs/operators';

@Component({
  selector: 'boo-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('searchInputComponent') searchInputComponent: ElementRef;

  readonly SearchRowCount = 6;
  readonly regexExp: RegExp = /([A-Za-z0-9]){2}/g;

  public _searchTerm = '';
  get searchTerm() {
    return this._searchTerm;
  }

  set searchTerm(value) {
    if (value.length > 1) {
      this.showResult = true;
    } else {
      this.showResult = false;
      this.results = [];
    }
    this._searchTerm = value;
  }

  public showResult = false;
  public results = [];
  private subs = [];
  public loading = false;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const obs = fromEvent(this.searchInputComponent.nativeElement, 'input').pipe(
      map(
        (e: any) => {
          return this.searchTerm;
        }
      ),
      filter((text: string) => text && text.length > 1),
      debounceTime(400),
      map((query: string) => {
        return of(query);
      }
      ),
      switchAll()
    );

    this.subs.push(
      obs.subscribe(data => {
        if (this.validateSearchTerm()) {
          this.getSearchResults();
        }
      })
    );


    // hide - onfocusout??
    const windowClickObservable = fromEvent(window, 'click');
    this.subs.push(
      windowClickObservable.subscribe((data: any) => {
        if (data.target.id === 'searchInput') { return; }
        this.showResult = false;
      })
    );



    const KeyPressObservable = fromEvent(window, 'keydown');
    KeyPressObservable.subscribe((data: KeyboardEvent) => {
      switch (data.key) {
        case 'ArrowDown':

          break;
        case 'ArrowUp':

          break;
        default:
          break;
      }
    });

    const clickObservable = fromEvent(this.searchInputComponent.nativeElement, 'click');

    this.subs.push(
      clickObservable.subscribe(data => {
        if (this.searchTerm.length > 1) {
          this.showResult = true;
        } else {
          this.showResult = false;
        }
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(item => {
      item.unsubscribe();
    });
  }


  private validateSearchTerm(): boolean {
    return this.regexExp.test(this.searchTerm);
  }

  private getSearchResults(): void {
    this.loading = true;
    this.apiService.get(this.SearchRowCount, this.searchTerm).subscribe(data => {
      this.loading = false;
      this.results = data.results.docs;
    });
  }


  public onItemClick(item) {
    this.searchTerm = `${item.name}${item.region ? ', ' + item.region : ''}${item.country ? ', ' + item.country : ''}`
    this.showResult = false;
  }
}
