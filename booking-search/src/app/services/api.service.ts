import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class ApiService {
  private requestUrl = "https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?";

  constructor(private http: HttpClient) { }

  public get(itemCount: number, searchString: string): Observable<any> {
    let options: any = {
      params: {
        'solrTerm': searchString,
        'solrRows': itemCount,
        'solrIndex': 'fts_en'
      }
    };

    return this.http.get(this.requestUrl, options); 
  }

}
